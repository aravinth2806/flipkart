// import 'package:flipkart_model/Nav_Bar/bloc/navscreen_bloc.dart';
// import 'package:flipkart_model/http/api.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';

// class NavScreen extends StatefulWidget {
//   const NavScreen() : super();
//   @override
//   _NavScreenState createState() => _NavScreenState();
// }

// class _NavScreenState extends State<NavScreen> {
//   late NavScreenBloc bloc;

//   @override
//   void initState() {
//     bloc = BlocProvider.of<NavScreenBloc>(context);
//     super.initState();
//   }

//   @override
//   void dispose() {
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     // var size = MediaQuery.of(context).size;
//     return BlocBuilder<NavScreenBloc, NavScreenState>(
//       bloc: bloc,
//       builder: (BuildContext context, NavScreenState state) {
//         if (state is NavScreenInitialState) {
//           Center(
//             child: Text('progressbar loading ....'),
//           );
//         } else if (state is NavScreenOnTapSuccessState) {
//           return BlocListener<NavScreenBloc, NavScreenState>(
//             bloc: bloc,
//             listener: (BuildContext context, NavScreenState state) {
//               print("bloc listner for nav screen..");
//             },
//           );
//         }
//         print("NavScreen worked");
//         // child:

//         return SafeArea(
//           child: ListView(
//             padding: EdgeInsets.zero,
//             children: <Widget>[
//               Container(
//                 // height: size.height / 10,
//                 color: Color(0xff2874F0),
//                 child: Center(
//                   child: ListTile(
//                     title: Text(
//                       'Home',
//                       style: TextStyle(color: Colors.white),
//                     ),
//                     leading: Icon(
//                       Icons.home,
//                       color: Colors.white,
//                     ),
//                     trailing: Image.network(
//                       'https://img1a.flixcart.com/www/linchpin/batman-returns/images/logo_lite-cbb357.png',
//                       // height: size.height / 20,
//                       scale: 1,
//                     ),
//                   ),
//                 ),
//               ),
//               Container(
//                 child: ListView.builder(
//                     itemCount: bloc.drawers.length,
//                     shrinkWrap: true,
//                     itemBuilder: (BuildContext context, int i) {
//                       print("ListViewbuilder worked");
//                       print(bloc.drawers[i].name.toString());
//                       return ListTile(
//                         leading: Icon(Icons.input),
//                         title: Text('listview worked',
//                             style: TextStyle(fontSize: 1)),
//                         //     Text(
//                         //   bloc.drawers[i].name.toString(),
//                         //   // style: TextStyle(fontSize: )
//                         // ),
//                       );
//                     }),
//               )
//               // ListTile(
//               //   leading: Icon(Icons.border_color),
//               //   title: Text('My Chats'),
//               //   onTap: () => {},
//               // );

//               // ListTile(
//               //   leading: Icon(Icons.exit_to_app),
//               //   title: Text('Logout'),
//               //   // onTap: () => {bloc.add(NavscreenLogoutEvent())},
//               // ),
//             ],
//           ),
//         );
//       },
//     );
//   }
// }
