import 'package:json_annotation/json_annotation.dart';

part 'category_item.g.dart';

@JsonSerializable()
class CategoryItem {
  @JsonKey(name: 'image')
  String? image;
  @JsonKey(name: 'name')
  String? name;

  CategoryItem({this.image, this.name});

  factory CategoryItem.fromJson(Map<String, dynamic> map) =>
      _$CategoryItemFromJson(map);

  Map<String, dynamic> toJson() => _$CategoryItemToJson(this);
}
