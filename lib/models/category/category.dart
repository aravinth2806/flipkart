import 'package:flipkart_model/models/category/category_item.dart';
import 'package:json_annotation/json_annotation.dart';

part 'category.g.dart';

@JsonSerializable()
class Category {
  List<CategoryItem>? data;

  Category({this.data});

  factory Category.fromJson(Map<String, dynamic> map) =>
      _$CategoryFromJson(map);

  Map<String, dynamic> toJson() => _$CategoryToJson(this);
}
