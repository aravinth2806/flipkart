// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'offer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Offer _$OfferFromJson(Map<String, dynamic> json) {
  return Offer(
    data: (json['data'] as List<dynamic>?)
        ?.map((e) => OfferItem.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$OfferToJson(Offer instance) => <String, dynamic>{
      'data': instance.data,
    };
