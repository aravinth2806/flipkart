import 'package:flipkart_model/models/offer/offer_item.dart';

import 'package:json_annotation/json_annotation.dart';

part 'offer.g.dart';

@JsonSerializable()
class Offer {
  List<OfferItem>? data;

  Offer({this.data});

  factory Offer.fromJson(Map<String, dynamic> map) => _$OfferFromJson(map);

  Map<String, dynamic> toJson() => _$OfferToJson(this);
}
