import 'package:json_annotation/json_annotation.dart';

part 'offer_item.g.dart';

@JsonSerializable()
class OfferItem {
  @JsonKey(name: 'image1')
  String? image1;
  @JsonKey(name: 'image2')
  String? image2;
  @JsonKey(name: 'image3')
  String? image3;

  OfferItem({this.image1, this.image2, this.image3});

  factory OfferItem.fromJson(Map<String, dynamic> map) =>
      _$OfferItemFromJson(map);

  Map<String, dynamic> toJson() => _$OfferItemToJson(this);
}
