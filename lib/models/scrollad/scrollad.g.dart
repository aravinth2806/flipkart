// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'scrollad.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ScrollAd _$ScrollAdFromJson(Map<String, dynamic> json) {
  return ScrollAd(
    data: (json['data'] as List<dynamic>?)
        ?.map((e) => ScrollAdItem.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$ScrollAdToJson(ScrollAd instance) => <String, dynamic>{
      'data': instance.data,
    };
