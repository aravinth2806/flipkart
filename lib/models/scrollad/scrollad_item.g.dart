// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'scrollad_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ScrollAdItem _$ScrollAdItemFromJson(Map<String, dynamic> json) {
  return ScrollAdItem(
    image: json['image'] as String?,
  );
}

Map<String, dynamic> _$ScrollAdItemToJson(ScrollAdItem instance) =>
    <String, dynamic>{
      'image': instance.image,
    };
