import 'package:flipkart_model/models/scrollad/scrollad_item.dart';
import 'package:json_annotation/json_annotation.dart';

part 'scrollad.g.dart';

@JsonSerializable()
class ScrollAd {
  List<ScrollAdItem>? data;

  ScrollAd({this.data});

  factory ScrollAd.fromJson(Map<String, dynamic> map) =>
      _$ScrollAdFromJson(map);

  Map<String, dynamic> toJson() => _$ScrollAdToJson(this);
}
