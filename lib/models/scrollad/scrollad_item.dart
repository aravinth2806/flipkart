import 'package:json_annotation/json_annotation.dart';

part 'scrollad_item.g.dart';

@JsonSerializable()
class ScrollAdItem {
  @JsonKey(name: 'image')
  String? image;

  ScrollAdItem({this.image});

  factory ScrollAdItem.fromJson(Map<String, dynamic> map) =>
      _$ScrollAdItemFromJson(map);

  Map<String, dynamic> toJson() => _$ScrollAdItemToJson(this);
}
