import 'package:json_annotation/json_annotation.dart';

part 'todaydeal_item.g.dart';

@JsonSerializable()
class TodayDealItem {
  @JsonKey(name: 'image1')
  String? image1;
  @JsonKey(name: 'image2')
  String? image2;
  @JsonKey(name: 'image3')
  String? image3;

  TodayDealItem({this.image1, this.image2, this.image3});

  factory TodayDealItem.fromJson(Map<String, dynamic> map) =>
      _$TodayDealItemFromJson(map);

  Map<String, dynamic> toJson() => _$TodayDealItemToJson(this);
}
