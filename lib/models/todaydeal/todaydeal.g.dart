// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'todaydeal.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TodayDeal _$TodayDealFromJson(Map<String, dynamic> json) {
  return TodayDeal(
    data: (json['data'] as List<dynamic>?)
        ?.map((e) => TodayDealItem.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$TodayDealToJson(TodayDeal instance) => <String, dynamic>{
      'data': instance.data,
    };
