import 'package:flipkart_model/models/todaydeal/todaydeal_item.dart';

import 'package:json_annotation/json_annotation.dart';

part 'todaydeal.g.dart';

@JsonSerializable()
class TodayDeal {
  List<TodayDealItem>? data;

  TodayDeal({this.data});

  factory TodayDeal.fromJson(Map<String, dynamic> map) =>
      _$TodayDealFromJson(map);

  Map<String, dynamic> toJson() => _$TodayDealToJson(this);
}
