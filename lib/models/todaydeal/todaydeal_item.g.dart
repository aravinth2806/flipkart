// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'todaydeal_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TodayDealItem _$TodayDealItemFromJson(Map<String, dynamic> json) {
  return TodayDealItem(
    image1: json['image1'] as String?,
    image2: json['image2'] as String?,
    image3: json['image3'] as String?,
  );
}

Map<String, dynamic> _$TodayDealItemToJson(TodayDealItem instance) =>
    <String, dynamic>{
      'image1': instance.image1,
      'image2': instance.image2,
      'image3': instance.image3,
    };
