// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feautrebrand.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeautreBrand _$FeautreBrandFromJson(Map<String, dynamic> json) {
  return FeautreBrand(
    data: (json['data'] as List<dynamic>?)
        ?.map((e) => FeautreBrandItem.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$FeautreBrandToJson(FeautreBrand instance) =>
    <String, dynamic>{
      'data': instance.data,
    };
