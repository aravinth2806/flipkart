// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feautrebrand_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeautreBrandItem _$FeautreBrandItemFromJson(Map<String, dynamic> json) {
  return FeautreBrandItem(
    image: json['image'] as String?,
  );
}

Map<String, dynamic> _$FeautreBrandItemToJson(FeautreBrandItem instance) =>
    <String, dynamic>{
      'image': instance.image,
    };
