import 'package:flipkart_model/models/feautrebrand/feautrebrand_item.dart';
import 'package:json_annotation/json_annotation.dart';

part 'feautrebrand.g.dart';

@JsonSerializable()
class FeautreBrand {
  List<FeautreBrandItem>? data;

  FeautreBrand({this.data});

  factory FeautreBrand.fromJson(Map<String, dynamic> map) =>
      _$FeautreBrandFromJson(map);

  Map<String, dynamic> toJson() => _$FeautreBrandToJson(this);
}
