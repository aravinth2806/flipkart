import 'package:json_annotation/json_annotation.dart';

part 'feautrebrand_item.g.dart';

@JsonSerializable()
class FeautreBrandItem {
  @JsonKey(name: 'image')
  String? image;

  FeautreBrandItem({this.image});

  factory FeautreBrandItem.fromJson(Map<String, dynamic> map) =>
      _$FeautreBrandItemFromJson(map);

  Map<String, dynamic> toJson() => _$FeautreBrandItemToJson(this);
}
