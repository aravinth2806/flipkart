import 'package:flipkart_model/models/drawers/drawer_item.dart';
import 'package:json_annotation/json_annotation.dart';

part 'drawers.g.dart';

@JsonSerializable()
class Drawers {
  List<DrawerItem>? data;

  Drawers({this.data});

  factory Drawers.fromJson(Map<String, dynamic> map) => _$DrawersFromJson(map);

  Map<String, dynamic> toJson() => _$DrawersToJson(this);
}
