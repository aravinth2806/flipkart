import 'package:json_annotation/json_annotation.dart';

part 'drawer_item.g.dart';

@JsonSerializable()
class DrawerItem {
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'icon')
  String? icon;

  DrawerItem({this.name, this.icon});

  factory DrawerItem.fromJson(Map<String, dynamic> map) =>
      _$DrawerItemFromJson(map);

  Map<String, dynamic> toJson() => _$DrawerItemToJson(this);
}
