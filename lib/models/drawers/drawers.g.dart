// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'drawers.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Drawers _$DrawersFromJson(Map<String, dynamic> json) {
  return Drawers(
    data: (json['data'] as List<dynamic>?)
        ?.map((e) => DrawerItem.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$DrawersToJson(Drawers instance) => <String, dynamic>{
      'data': instance.data,
    };
