// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'drawer_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DrawerItem _$DrawerItemFromJson(Map<String, dynamic> json) {
  return DrawerItem(
    name: json['name'] as String?,
    icon: json['icon'] as String?,
  );
}

Map<String, dynamic> _$DrawerItemToJson(DrawerItem instance) =>
    <String, dynamic>{
      'name': instance.name,
      'icon': instance.icon,
    };
