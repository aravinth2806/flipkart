import 'package:json_annotation/json_annotation.dart';

part 'dealoftheday_item.g.dart';

@JsonSerializable()
class DealOfTheDayItem {
  @JsonKey(name: 'image')
  String? image;
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'discount')
  String? discount;

  DealOfTheDayItem({this.image, this.name, this.discount});

  factory DealOfTheDayItem.fromJson(Map<String, dynamic> map) =>
      _$DealOfTheDayItemFromJson(map);

  Map<String, dynamic> toJson() => _$DealOfTheDayItemToJson(this);
}
