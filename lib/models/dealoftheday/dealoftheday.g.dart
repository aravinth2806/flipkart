// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dealoftheday.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DealOfTheDay _$DealOfTheDayFromJson(Map<String, dynamic> json) {
  return DealOfTheDay(
    data: (json['data'] as List<dynamic>?)
        ?.map((e) => DealOfTheDayItem.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$DealOfTheDayToJson(DealOfTheDay instance) =>
    <String, dynamic>{
      'data': instance.data,
    };
