import 'package:flipkart_model/models/dealoftheday/dealoftheday_item.dart';

import 'package:json_annotation/json_annotation.dart';

part 'dealoftheday.g.dart';

@JsonSerializable()
class DealOfTheDay {
  List<DealOfTheDayItem>? data;

  DealOfTheDay({this.data});

  factory DealOfTheDay.fromJson(Map<String, dynamic> map) =>
      _$DealOfTheDayFromJson(map);

  Map<String, dynamic> toJson() => _$DealOfTheDayToJson(this);
}
