// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dealoftheday_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DealOfTheDayItem _$DealOfTheDayItemFromJson(Map<String, dynamic> json) {
  return DealOfTheDayItem(
    image: json['image'] as String?,
    name: json['name'] as String?,
    discount: json['discount'] as String?,
  );
}

Map<String, dynamic> _$DealOfTheDayItemToJson(DealOfTheDayItem instance) =>
    <String, dynamic>{
      'image': instance.image,
      'name': instance.name,
      'discount': instance.discount,
    };
