import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebView extends StatefulWidget {
  WebView({String? initialUrl, JavascriptMode? javascriptMode, key}) : super();

  @override
  _WebViewState createState() => _WebViewState();
}

class _WebViewState extends State<WebView> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: WebView(
            // key: _key,
            javascriptMode: JavascriptMode.unrestricted,
            initialUrl: "https://camellabs.com/"));
  }
}
