import 'package:flutter/material.dart';

class TodayDeal extends StatefulWidget {
  TodayDeal() : super();

  @override
  _TodayDealState createState() => _TodayDealState();
}

class _TodayDealState extends State<TodayDeal> {
  @override
  Widget build(BuildContext context) {
    return Container(
        // height: MediaQuery.of(context).size.height,
        child: Column(children: <Widget>[
      Padding(
        padding: const EdgeInsets.all(10),
        child: Text(
          'Today Deals',
          style: TextStyle(fontSize: 20),
        ),
      ),
      Card(
        child: Column(
          children: [
            Row(
              children: <Widget>[
                Expanded(
                  child: Image.network(
                      'https://rukminim1.flixcart.com/flap/400/610/image/14d50e90c8413811.jpg?q=90'),
                ),
                Expanded(
                  child: Image.network(
                      'https://rukminim1.flixcart.com/flap/400/610/image/fe6fb023f400c86f.jpg?q=90'),
                ),
                // Expanded(
                //   child: Image.network(
                //       'https://rukminim1.flixcart.com/flap/400/610/image/14d50e90c8413811.jpg?q=90'),
                // ),
              ],
            ),
          ],
        ),
      ),
      Card(
        child: Column(
          children: [
            Row(
              children: <Widget>[
                Expanded(
                  child: Image.network(
                      'https://rukminim1.flixcart.com/flap/400/610/image/14d50e90c8413811.jpg?q=90'),
                ),
                Expanded(
                  child: Image.network(
                      'https://rukminim1.flixcart.com/flap/400/610/image/fe6fb023f400c86f.jpg?q=90'),
                ),
                Expanded(
                  child: Image.network(
                      'https://rukminim1.flixcart.com/flap/400/610/image/fe6fb023f400c86f.jpg?q=90'),
                ),
                Expanded(
                  child: Image.network(
                      'https://rukminim1.flixcart.com/flap/400/610/image/14d50e90c8413811.jpg?q=90'),
                ),
              ],
            ),
          ],
        ),
      ),
    ]));
  }
}
