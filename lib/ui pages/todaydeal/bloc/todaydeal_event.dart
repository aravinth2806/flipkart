part of 'todaydeal_bloc.dart';

abstract class TodaydealEvent extends Equatable {
  const TodaydealEvent();

  @override
  List<Object> get props => [];
}
