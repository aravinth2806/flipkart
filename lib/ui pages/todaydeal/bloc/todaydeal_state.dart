part of 'todaydeal_bloc.dart';

abstract class TodaydealState extends Equatable {
  const TodaydealState();
  
  @override
  List<Object> get props => [];
}

class TodaydealInitial extends TodaydealState {}
