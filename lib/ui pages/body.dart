import 'package:flipkart_model/ui%20pages/category/category.dart';
import 'package:flipkart_model/ui%20pages/dealoftheday/dealofday.dart';
// import 'package:flipkart_model/ui%20pages/dealoftheday/dealoftheday.dart';
import 'package:flipkart_model/ui%20pages/feauture_brand/feautre_brand.dart';
import 'package:flipkart_model/ui%20pages/mobiles/mobile.dart';
import 'package:flipkart_model/ui%20pages/offer_view/offer_view.dart';
import 'package:flipkart_model/ui%20pages/scroll_ad/scroll_ad.dart';
import 'package:flipkart_model/ui%20pages/todaydeal/todaydeal.dart';
import 'package:flipkart_model/web.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          CategoryList(),
          SizedBox(
            height: 2,
          ),
          ScrollAd(),
          OfferView(),
          DealOfTheDayList(),
          FeautreBrand(),
          TodayDeal(),
          MobileView(),
          // SizedBox(
          //   height: 5,
          // ),
          // WebView(),
        ],
      ),
    );
  }
}
