part of 'dealoftheday_bloc.dart';

abstract class DealofthedayEvent extends Equatable {
  const DealofthedayEvent();

  @override
  List<Object> get props => [];
}
