part of 'dealoftheday_bloc.dart';

abstract class DealofthedayState extends Equatable {
  const DealofthedayState();
  
  @override
  List<Object> get props => [];
}

class DealofthedayInitial extends DealofthedayState {}
