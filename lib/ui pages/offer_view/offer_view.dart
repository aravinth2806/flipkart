import 'package:flutter/material.dart';

class OfferView extends StatefulWidget {
  OfferView() : super();

  @override
  _OfferViewState createState() => _OfferViewState();
}

class _OfferViewState extends State<OfferView> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height / 5,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              child: Image.network(
                  'https://rukminim1.flixcart.com/flap/267/284/image/98cc1dc1cf46f8b2.jpg?q=90'),
            ),
          ),
          Expanded(
            child: Container(
              child: Image.network(
                  'https://rukminim1.flixcart.com/flap/267/284/image/2c958a68d47f14df.jpg?q=90'),
            ),
          ),
          Expanded(
            child: Container(
              child: Image.network(
                  'https://rukminim1.flixcart.com/flap/267/284/image/46424bd07a6d488d.jpg?q=90'),
            ),
          ),
        ],
      ),
    );
  }
}
