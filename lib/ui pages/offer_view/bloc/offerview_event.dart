part of 'offerview_bloc.dart';

abstract class OfferviewEvent extends Equatable {
  const OfferviewEvent();

  @override
  List<Object> get props => [];
}
