part of 'offerview_bloc.dart';

abstract class OfferviewState extends Equatable {
  const OfferviewState();
  
  @override
  List<Object> get props => [];
}

class OfferviewInitial extends OfferviewState {}
