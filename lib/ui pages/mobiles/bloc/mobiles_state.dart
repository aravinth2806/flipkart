part of 'mobiles_bloc.dart';

abstract class MobilesState extends Equatable {
  const MobilesState();
  
  @override
  List<Object> get props => [];
}

class MobilesInitial extends MobilesState {}
