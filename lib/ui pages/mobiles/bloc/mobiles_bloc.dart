import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'mobiles_event.dart';
part 'mobiles_state.dart';

class MobilesBloc extends Bloc<MobilesEvent, MobilesState> {
  MobilesBloc() : super(MobilesInitial()) {
    on<MobilesEvent>((event, emit) {
      // TODO: implement event handler
    });
  }
}
