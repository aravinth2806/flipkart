part of 'mobiles_bloc.dart';

abstract class MobilesEvent extends Equatable {
  const MobilesEvent();

  @override
  List<Object> get props => [];
}
