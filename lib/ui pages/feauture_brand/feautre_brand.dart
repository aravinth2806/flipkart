import 'package:flutter/material.dart';

class FeautreBrand extends StatefulWidget {
  FeautreBrand() : super();

  @override
  _FeautreBrandState createState() => _FeautreBrandState();
}

class _FeautreBrandState extends State<FeautreBrand> {
  @override
  Widget build(BuildContext context) {
    return Container(
      // height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: FittedBox(
        child: Card(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 20, bottom: 8, top: 8),
                child: Text(
                  'Featured Brand',
                  style: TextStyle(fontSize: 20),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20, bottom: 8),
                child: Text(
                  'Sponsored',
                  style: TextStyle(color: Colors.grey),
                ),
              ),
              Image.network(
                'https://rukminim1.flixcart.com/flap/764/410/image/70708f6b3f39b807.jpg?q=90',
                height: MediaQuery.of(context).size.height / 3,
                width: MediaQuery.of(context).size.width,
              ),
              Image.network(
                'https://rukminim1.flixcart.com/flap/764/410/image/70708f6b3f39b807.jpg?q=90',
                height: MediaQuery.of(context).size.height / 3,
                width: MediaQuery.of(context).size.width,
              )
            ],
          ),
        ),
      ),
    );
  }
}
