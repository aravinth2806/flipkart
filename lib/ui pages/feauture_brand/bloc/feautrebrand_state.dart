part of 'feautrebrand_bloc.dart';

abstract class FeautrebrandState extends Equatable {
  const FeautrebrandState();
  
  @override
  List<Object> get props => [];
}

class FeautrebrandInitial extends FeautrebrandState {}
