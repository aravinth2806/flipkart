part of 'feautrebrand_bloc.dart';

abstract class FeautrebrandEvent extends Equatable {
  const FeautrebrandEvent();

  @override
  List<Object> get props => [];
}
