import 'package:equatable/equatable.dart';
// import 'package:hydrated_bloc/hydrated_bloc.dart';

class NavScreenState extends Equatable {
  @override
  List<Object?> get props => [NavScreenSuccessState()];
}

class NavScreenInitialState extends NavScreenState {
  @override
  bool operator ==(Object other) => false;
}

class NavScreenSuccessState extends NavScreenState {
  @override
  bool operator ==(Object other) => false;
}

class NavScreenLogoutState extends NavScreenState {
  @override
  bool operator ==(Object other) => false;
}

class NavScreenLogoutSuccessState extends NavScreenState {
  @override
  bool operator ==(Object other) => false;
}
