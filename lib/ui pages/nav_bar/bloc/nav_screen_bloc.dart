import 'dart:async';

import 'package:flipkart_model/http/api.dart';
import 'package:flipkart_model/models/drawers/drawer_item.dart';
import 'package:flipkart_model/models/drawers/drawers.dart';
import 'package:flipkart_model/ui%20pages/nav_bar/bloc/nav_screen_event.dart';
import 'package:flipkart_model/ui%20pages/nav_bar/bloc/nav_screen_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NavScreenBloc extends Bloc<NavScreenEvent, NavScreenState> {
  NavScreenBloc() : super(NavScreenInitialState());
  List<DrawerItem> drawers = [];
  @override
  Stream<NavScreenState> mapEventToState(NavScreenEvent event) async* {
    if (event is NavScreenInitialEvent) {
      drawers = (await DioClient.getDrawerLists()) as List<DrawerItem>;
      drawers.addAll(<DrawerItem>[]);
      print("bloc page is worked");
      // names = [
      //   'header',
      //   'body',
      //   'footer',
      //   'drawers',
      //   'navbar',
      //   'hhhh',
      //   'jfrtgfg'
      // ];
      yield NavScreenSuccessState();
    }
    if (event is NavScreenLogoutEvent) {
      yield NavScreenLogoutSuccessState();
    }
  }
}
