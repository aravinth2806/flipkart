import 'package:equatable/equatable.dart';

class NavScreenEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class NavScreenInitialEvent extends NavScreenEvent {}

class NavScreenLogoutEvent extends NavScreenEvent {}

class NavScreenLogoutSuccessEvent extends NavScreenEvent {}
