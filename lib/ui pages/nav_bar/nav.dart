import 'dart:core';

import 'package:flipkart_model/ui%20pages/nav_bar/bloc/nav_screen_bloc.dart';
import 'package:flipkart_model/ui%20pages/nav_bar/bloc/nav_screen_event.dart';
import 'package:flipkart_model/ui%20pages/nav_bar/bloc/nav_screen_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NavScreen1 extends StatefulWidget {
  const NavScreen1() : super();
  @override
  _NavScreen1State createState() => _NavScreen1State();
}

class _NavScreen1State extends State<NavScreen1> {
  late NavScreenBloc bloc;

  @override
  void initState() {
    bloc = BlocProvider.of<NavScreenBloc>(context)
      ..add(NavScreenInitialEvent());
    super.initState();
  }

  // @override
  // void dispose() {
  //   super.dispose();
  // }
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NavScreenBloc, NavScreenState>(
      bloc: BlocProvider.of<NavScreenBloc>(context),
      builder: (BuildContext context, NavScreenState state) {
        if (state is NavScreenInitialState) {
          return Center(
            child: Text('Loading...'),
          );
        } else if (state is NavScreenSuccessState) {
          return SafeArea(
            child: Drawer(
              child: ListView(
                padding: EdgeInsets.zero,
                children: [
                  Container(
                    color: Color(0xff2874F0),
                    child: Center(
                      child: ListTile(
                        title: Text(
                          'Home',
                          style: TextStyle(color: Colors.white),
                        ),
                        leading: Icon(
                          Icons.home,
                          color: Colors.white,
                        ),
                        trailing: Image.network(
                          'https://img1a.flixcart.com/www/linchpin/batman-returns/images/logo_lite-cbb357.png',
                          // height: size.height / 20,
                          scale: 1,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    child: ListView.builder(
                        itemCount: bloc.drawers.length,
                        shrinkWrap: true,
                        itemBuilder: (BuildContext context, int i) {
                          return ListTile(
                            leading: Icon(Icons.list),
                            title: Text(bloc.drawers[i].name.toString(),
                                style: TextStyle(fontSize: 15)),
                          );
                        }),
                  ),
                ],
              ),
            ),
          );
        } else {
          return Text('');
        }
      },
    );
  }
}
