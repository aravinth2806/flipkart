// import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'scroll_ad_event.dart';
part 'scroll_ad_state.dart';

class ScrollAdBloc extends Bloc<ScrollAdEvent, ScrollAdState> {
  ScrollAdBloc() : super(ScrollAdInitialState());
  var names = [
    'https://rukminim1.flixcart.com/flap/656/352/image/2fdcb26eedf83b2f.jpg?q=90',
    'https://pbs.twimg.com/media/CKmOzB0VEAAAm6U.jpg:large',
    'https://rukminim1.flixcart.com/flap/656/352/image/2fdcb26eedf83b2f.jpg?q=90',
    'https://rukminim1.flixcart.com/flap/132/124/image/0574df.jpg?q=90',
    'https://rukminim1.flixcart.com/flap/132/124/image/a36b7b1c84e8203a.jpg?q=90',
    'https://rukminim1.flixcart.com/flap/132/124/image/b3af3e.jpg?q=90',
    'https://rukminim1.flixcart.com/flap/132/124/image/fb63f3.jpg?q=90',
  ];
  @override
  Stream<ScrollAdState> mapEventToState(ScrollAdEvent event) async* {
    if (event is ScrollAdInitialEvent) {
      names.length = names as int;
      yield ScrollAdSuccessState();
    }

    if (event is ScrollAdOnTapEvent) {
      yield ScrollAdOnTapSuccessState();
    }
  }
}
