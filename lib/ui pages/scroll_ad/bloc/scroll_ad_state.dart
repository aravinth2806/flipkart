part of 'scroll_ad_bloc.dart';

// abstract class ScrollAdState extends Equatable {
//   const ScrollAdState();

//   @override
//   List<Object> get props => [];
// }

// class ScrollAdInitial extends ScrollAdState {}
class ScrollAdState extends Equatable {
  @override
  List<Object?> get props => [];
}

class ScrollAdInitialState extends ScrollAdState {}

class ScrollAdSuccessState extends ScrollAdState {}

class ScrollAdOnTapSuccessState extends ScrollAdState {
  @override
  bool operator ==(Object other) => false;
}

class ScrollAdOnTapErrorState extends ScrollAdState {
  @override
  bool operator ==(Object other) => false;
}
