part of 'scroll_ad_bloc.dart';

class ScrollAdEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class ScrollAdInitialEvent extends ScrollAdEvent {}

class ScrollAdOnTapEvent extends ScrollAdEvent {}
