// import 'package:flipkart_model/ui%20pages/scroll_ad/bloc/bloc/scroll_ad_bloc.dart';
import 'package:flutter/material.dart';

class ScrollAd extends StatefulWidget {
  ScrollAd() : super();

  @override
  _ScrollAdState createState() => _ScrollAdState();
}

class _ScrollAdState extends State<ScrollAd> {
  // late ScrollAdBloc bloc12;
  var names = [
    'https://rukminim1.flixcart.com/flap/656/352/image/2fdcb26eedf83b2f.jpg?q=90',
    'https://pbs.twimg.com/media/CKmOzB0VEAAAm6U.jpg:large',
    'https://rukminim1.flixcart.com/flap/656/352/image/2fdcb26eedf83b2f.jpg?q=90',
    'https://rukminim1.flixcart.com/flap/132/124/image/0574df.jpg?q=90',
    'https://rukminim1.flixcart.com/flap/132/124/image/a36b7b1c84e8203a.jpg?q=90',
    'https://rukminim1.flixcart.com/flap/132/124/image/b3af3e.jpg?q=90',
    'https://rukminim1.flixcart.com/flap/132/124/image/fb63f3.jpg?q=90',
  ];
  // @override
  // void initState() {
  //   bloc12 = BlocProvider.of(context);
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    return
        // BlocBuilder(
        //   bloc: bloc12,
        //   builder: (context, state) {
        //     if (state is ScrollAdInitialState) {
        //       print('ScrollAd data not pass');

        //       return Center(
        //         child: CircularProgressIndicator(),
        //       );
        //     } else {
        //       return BlocListener(
        //           bloc: bloc12,
        //           listener: (context, state) {
        //             if (state is ScrollAdOnTapSuccessState) {
        //               print('homescreen data  pass');
        //             }
        //           },
        //           child:
        Padding(
      padding: const EdgeInsets.all(3),
      child: Container(
        height: MediaQuery.of(context).size.height / 4,
        child: ListView.builder(
          itemCount: names.length,
          // itemCount: bloc12.names.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, i) {
            return Container(
              height: MediaQuery.of(context).size.height / 4,
              child: Padding(
                padding: const EdgeInsets.only(left: 2, right: 2),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(6),
                  child: Image.network(
                    names[i],
                    // bloc12.names[i],
                    // child: Image.network(
                    //   'https://rukminim1.flixcart.com/flap/656/352/image/2fdcb26eedf83b2f.jpg?q=90',
                    height: MediaQuery.of(context).size.height / 4,
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
// );

//
// }
// }
