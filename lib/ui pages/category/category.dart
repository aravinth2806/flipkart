import 'package:flipkart_model/models/deals.dart';
import 'package:flutter/material.dart';

class CategoryList extends StatefulWidget {
  CategoryList() : super();

  @override
  _CategoryListState createState() => _CategoryListState();
}

class _CategoryListState extends State<CategoryList> {
  late List<deals> deal;

  @override
  void initState() {
    super.initState();
    addCategoryItem();
  }

  addCategoryItem() {
    deal = <deals>[];
    deal.add(deals(
        'https://rukminim1.flixcart.com/flap/132/124/image/0ec0f8.jpg?q=90',
        'Offer Zone',
        '-'));
    deal.add(deals(
        'https://rukminim1.flixcart.com/flap/132/124/image/2af8b5e316c12e7d.jpg?q=90',
        'Grocery',
        '-'));
    deal.add(deals(
        'https://rukminim1.flixcart.com/flap/132/124/image/54abb23755dc69db.jpg?q=90',
        'Mobiles',
        '-'));
    deal.add(deals(
        'https://rukminim1.flixcart.com/flap/132/124/image/82733b7b3fef58d5.jpg?q=90',
        'Fashion',
        '-'));
    deal.add(deals(
        'https://rukminim1.flixcart.com/flap/132/124/image/e651df.jpg?q=90',
        'Electronics',
        '-'));
    deal.add(deals(
        'https://rukminim1.flixcart.com/flap/132/124/image/0574df.jpg?q=90',
        'Home',
        '-'));
    deal.add(deals(
        'https://rukminim1.flixcart.com/flap/132/124/image/a36b7b1c84e8203a.jpg?q=90',
        'Appliances',
        '-'));
    deal.add(deals(
        'https://rukminim1.flixcart.com/flap/132/124/image/b3af3e.jpg?q=90',
        'Toys & Baby',
        '-'));
    deal.add(deals(
        'https://rukminim1.flixcart.com/flap/132/124/image/fb63f3.jpg?q=90',
        ' Sports & More',
        '-'));
    deal.add(deals(
        'https://rukminim1.flixcart.com/flap/132/124/image/52d4da926dbea236.jpg?q=90',
        'Beauty',
        '-'));
  }

  buildItem(BuildContext context, int index) {
    return SizedBox(
      width: MediaQuery.of(context).size.width / 3.5,
      child: MaterialButton(
        // style: ElevatedButton.styleFrom(
        //   primary: Colors.white,
        //   // minimumSize: Size(100, 50),
        // ),
        onPressed: () {
          print("Category Taped...");
        },
        child: Container(
          height: MediaQuery.of(context).size.height / 5,
          width: MediaQuery.of(context).size.width / 4,
          child: Column(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width / 5,
                height: MediaQuery.of(context).size.height / 15,
                child: Image.network(deal[index].imageUrl),
              ),
              Text(
                deal[index].name,
                style: TextStyle(fontSize: 10, color: Colors.black),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // var size = MediaQuery.of(context).size;
    return Container(
        height: MediaQuery.of(context).size.height / 11,
        child: ListView.builder(
            itemCount: deal.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              return buildItem(context, index);
            }));
  }
}
