import 'dart:convert';
import 'package:flipkart_model/models/drawers/drawer_item.dart';
import 'package:flipkart_model/models/drawers/drawers.dart';
import 'dart:async';
import 'package:flutter/services.dart' as rootBundle;

class DioClient {
  static Future<Drawers> getDrawerList() async {
    String drawerList = await rootBundle.rootBundle.loadString(
      'jsonfile/drawer.json',
    );
    print("local json file is passed..");
    print(drawerList);
    // drawerList = await json.decode(drawerList);
    return Drawers.fromJson(await json.decode(drawerList));
  }

  static Future<Drawers> getDrawerLists() async {
//    final jsonCategory = await rootBundle.loadString("assets/CategoryList.json");
    final jsonCategory =
        await rootBundle.rootBundle.loadString('jsonfile/drawer.json');
    print(jsonCategory);
    final mapJsonCategory = Map<String, dynamic>.from(jsonDecode(jsonCategory));
    print(mapJsonCategory);
    return Drawers.fromJson(mapJsonCategory);
  }

  //* Future<String> _loadFromAsset() async {
  //   return await rootBundle.loadString('jsonfile/drawer.json');
  // }

  // Future parseJson() async {
  //   String jsonString = await _loadFromAsset();
  //   final jsonResponse = jsonDecode(jsonString);
  //* }

  // static Future<List<DrawerItem>> getDrawerLists() async {
  //   final jsondata =
  //       await rootBundle.rootBundle.loadString('jsonfile/drawer.json');
  //   final list = json.decode(jsondata) as List<dynamic>;
  //   return list.map((e) => DrawerItem.fromJson(e)).toList();
  // }

  // static Future<String> loadAsset() async {
  //   return await rootBundle.rootBundle.loadString('jsonfile/drawer.json');
  // }
}
