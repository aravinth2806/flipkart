import 'package:flipkart_model/ui%20pages/body.dart';
import 'package:flipkart_model/ui%20pages/nav_bar/bloc/nav_screen_bloc.dart';
import 'package:flipkart_model/ui%20pages/nav_bar/bloc/nav_screen_state.dart';
import 'package:flipkart_model/ui%20pages/nav_bar/nav.dart';
import 'package:flipkart_model/ui%20pages/nav_bar/nav_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage() : super();

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController textFieldController = TextEditingController();
  late NavScreenBloc bloc2;

  @override
  void initState() {
    // bloc2 = BlocProvider.of(context);
    bloc2 = NavScreenBloc();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<NavScreenBloc>(
        create: (BuildContext context) => bloc2,
        child: BlocListener<NavScreenBloc, NavScreenState>(
          listener: (BuildContext context, NavScreenState state) {
            setState(() {
              // Navigator.of(context).pushNamed('');
              // Navigator.of(context).pushNamed('');
              // Navigator.of(context).pushNamed('');
            });
          },
          child: BlocBuilder<NavScreenBloc, NavScreenState>(
            builder: (BuildContext context, NavScreenState state) => Scaffold(
              drawer: Drawer(child: NavScreen1()),
              body: NestedScrollView(
                headerSliverBuilder: (BuildContext context, bool) {
                  return <Widget>[
                    SliverAppBar(
                      elevation: 0,
                      automaticallyImplyLeading: true,
                      title: Image.asset(
                        'assets/fk-plus_043837.png',
                        height: MediaQuery.of(context).size.height / 2,
                        width: MediaQuery.of(context).size.width / 4,
                      ),
                      actions: [
                        IconButton(
                            onPressed: () {},
                            icon: Icon(Icons.notification_add)),
                        IconButton(
                            onPressed: () {}, icon: Icon(Icons.shopping_cart)),
                      ],
                      pinned: true,
                      floating: true,
                      forceElevated: true,
                      bottom:

                          // TabBar(
                          //   tabs: [
                          //     Tab(
                          //       icon: Icon(Icons.home),
                          //       text: 'Home',
                          //     ),
                          //   ],
                          // )

                          PreferredSize(
                        child: Padding(
                          padding: const EdgeInsets.all(10),
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            child: searchField(),
                          ),
                        ),
                        // preferredSize: Size.fromHeight(68),
                        preferredSize: Size.square(68),
                      ),
                    ),
                  ];
                },
                body: Body(),
              ),
              bottomNavigationBar: SizedBox(
                height: 50,
                child: BottomNavigationBar(
                  type: BottomNavigationBarType.fixed,
                  items: const <BottomNavigationBarItem>[
                    BottomNavigationBarItem(
                      icon: Icon(
                        Icons.home,
                        size: 20,
                      ),
                      label: 'Home',
                      backgroundColor: Colors.blue,
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(
                        Icons.shop,
                        size: 20,
                      ),
                      label: 'Shop',
                      backgroundColor: Colors.green,
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(
                        Icons.shopping_cart,
                        size: 20,
                      ),
                      label: 'Credit',
                      backgroundColor: Colors.purple,
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(
                        Icons.gamepad,
                        size: 20,
                      ),
                      label: 'Game Zone',
                      backgroundColor: Colors.pink,
                    ),
                  ],
                  // currentIndex: 3,
                  // selectedItemColor: Colors.amber[800],
                  // onTap: _onItemTapped,
                ),
              ),
            ),
          ),
        ));
  }

  Widget searchField() {
    print('HomePage worked');
    return TextField(
      autofocus: true,
      decoration: InputDecoration(
          hintText: 'Search for Products, Brands and More',
          hintStyle: TextStyle(
            color: Colors.grey,
            fontSize: 13,
          ),
          prefixIcon: Icon(Icons.search),
          suffixIcon: IconButton(
            icon: Icon(Icons.mic),
            onPressed: () {
              print('mic clicked');
            },
          ),
          fillColor: Colors.white,
          filled: true),
    );
  }
}
